The following API resources are currently available:

| Resource                        | Available endpoints |
|:--------------------------------|:--------------------|
| [Ping](#ping)                   | `/ping`             |
| [Master ID](#master_id)         | `/whoami`           |
| [Item Database](#item_database) | `/items/list`       |
| [Item Icons](#item_icons)       | `/items/icons`      |
| [Market List](#market_list)     | `/market/list`      |
| [Fame List](#fame_list)         | `/fame/list`        |


### Common Response Fields

All the API endpoints will return the following two values:

| Name                   | Type    | Value                                                                                     |
|:-----------------------|:--------|:------------------------------------------------------------------------------------------|
| `version`              | Integer | The API version. Currently always 1.                                                      |
| `generation_timestamp` | Date    | The timestamp the reply was generated at. Some endpoints return a cached reply by design. |

## Ping

> - **Authentication required**: No
> - **Rate limits**: 2 requests every 10 seconds

Responds with a pong message. This can be used to test the API availability.

```
GET /ping
```

```
curl https://api.arcadia-online.org/api/v1/ping
```

#### Response Fields

| Name      | Type   | Value         |
|:----------|:-------|:--------------|
| `message` | String | Always "pong" |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "message": "pong"
}
```

## Master ID

> - **Authentication required**: Yes
> - **Rate limits**: 2 requests every 10 seconds

Returns the Master ID associated to the used API Key. This can be used to test
the API authentication.

```
GET /whoami
```

```
curl --header "x-api-key: <your_api_key>" https://api.arcadia-online.org/api/v1/whoami
```

#### Response Fields

| Name        | Type    | Value                         |
|:------------|:--------|:------------------------------|
| `master_id` | Integer | The current user's Master ID. |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "master_id": 1
}
```

## Item Database

> - **Authentication required**: Yes
> - **Rate limits**: 6 requests every day
> - **Recommended limit**: The item database should be cached for at least 7 days. The database doesn't change very often.

Returns the simplified contents of the Arcadia Online item database.

```
GET /items/list
```

```
curl --header "x-api-key: <your_api_key>" https://api.arcadia-online.org/api/v1/items/list
```

#### Response Fields

| Name                   | Type      | Value                                                            |
|:-----------------------|:----------|:-----------------------------------------------------------------|
| `items`                | Array     | The item database.                                               |
| `items[i]`             | Object    | Each record in the item database.                                |
| `items[i]/item_id`     | Integer   | The unique item identifier, numeric.                             |
| `items[i]/unique_name` | String    | The unique item identifier, as a string.                         |
| `items[i]/name`        | String    | The user-visible item name.                                      |
| `items[i]/type`        | String    | The item type. Possible values are:<br><br>`IT_HEALING`: Healing items<br>`IT_UNKNOWN`: Unknown item type<br>`IT_USABLE`: Other usable items<br>`IT_ETC`: Misc items<br/>`IT_WEAPON`: Weapons<br>`IT_ARMOR`: Armors<br>`IT_CARD`: Cards<br>`IT_PETEGG`: Pet eggs<br>`IT_PETARMOR`: Pet accessories<br>`IT_UNKNOWN2`: Unknown items (unused)<br>`IT_AMMO`: Ammunitions<br>`IT_DELAYCONSUME`: Consumable items (triggering additional actions)<br>`IT_CASH`: Cash shop items |
| `items[i]/subtype`     | String?   | The item subtype. Only present when the item type is `IT_WEAPON` or `IT_AMMO`. Possible values are:<br><br>For `IT_WEAPON`:<br>`W_FIST`: Unknown weapon (unused)<br>`W_DAGGER`: Daggers<br>`W_1HSWORD`: 1-hand swords<br>`W_2HSWORD`: 2-hand swords<br>`W_1HSPEAR`: 1-hand spears<br>`W_2HSPEAR`: 2-hand spears<br>`W_1HAXE`: 1-hand axes<br>`W_2HAXE`: 2-hand axes<br>`W_MACE`: 1-hand maces<br>`W_2HMACE`: 2-hand maces<br>`W_STAFF`: Staves<br>`W_2HSTAFF`: 2-hand staves<br>`W_BOW`: Bows<br>`W_KNUCKLE`: Knuckles<br>`W_MUSICAL`: Musical instruments<br>`W_WHIP`: Whips<br>`W_BOOK`: Books<br>`W_KATAR`: Katars<br>`W_REVOLVER`: Pistols<br>`W_RIFLE`: Rifles<br>`W_GATLING`: Gatling guns<br>`W_SHOTGUN`: Shotguns<br>`W_GRENADE`: Grenades<br>`W_HUUMA`: Huuma shuriken<br><br>For `IT_AMMO`:<br>`A_ARROW`: Arrows<br>`A_DAGGER`: Throwing daggers<br>`A_BULLET`: Bullets<br>`A_SHELL`: Shells<br>`A_GRENADE`: Grenades<br>`A_SHURIKEN`: Shuriken<br>`A_KUNAI`: Kunai<br>`A_CANNONBALL`: Cannon balls<br>`A_THROWWEAPON`: Other throwing weapons |
| `items[i]/npc_price`   | Integer   | NPC buying price (or twice the NPC selling price if the item cannot be purchased from an NPC. |
| `items[i]/slots`       | Integer?  | Amount of slots. Only present if the item has at least one slot. |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "items": [
        {
            "item_id": 501,
            "unique_name": "Red_Potion",
            "name": "Red Potion",
            "type": "IT_HEALING",
            "npc_price": 50
        },
        {
            "item_id": 1102,
            "unique_name": "Sword_",
            "name": "Sword",
            "type": "IT_WEAPON",
            "subtype": "W_1HSWORD",
            "npc_price": 100,
            "slots": 4
        }
    ]
}
```

## Item Icons

> - **Authentication required**: Yes
> - **Rate limits**: 6 requests every day
> - **Recommended limit**: The item icon database should be cached for at least 7 days. The database doesn't change very often.

Returns the simplified contents of the mini-icons for the Arcadia Online item database.

```
GET /items/icons
```

```
curl --header "x-api-key: <your_api_key>" https://api.arcadia-online.org/api/v1/items/icons
```

#### Response Fields

| Name               | Type      | Value                         |
|:-------------------|:----------|:------------------------------|
| `icons`            | Array     | The item icons list.          |
| `icons[i]`         | Object    | Each record in the item database. Note: the amount of records may be less than the amount of item database records, in case some items don't have an icon available. |
| `icons[i]/item_id` | Integer   | The unique item identifier, numeric. Matches the `item_id` field of the [Item Database](#item_db). |
| `icons[i]/icon`    | String    | The item icon, as a data URI. |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "items": [
        {
            "item_id": 501,
            "icon": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAP1BMVEX/AP/XqpHDkHqudmL////ox6X/9LxJSUm3t7eSkpLb29tubm7Kt6//8+u+QzDTjG7tZEfyhVmPIhi4o5v/8OVQ35yOAAAAAXRSTlMAQObYZgAAAAFiS0dEBI9o2VEAAAAHdElNRQfjCwIOOR3mwfZ/AAAAmElEQVQoz7WP2w6DIBAFsSyedavLRf//W7uQVmk0afvQeSIzOQSc+wfDcPOXnigE/1MYxxBw0szTYAMwv2kikunuPcBEc+8XknkCVNXOfHhADOYYVYG9mE92V2y08iksQBbRp+/CDM3CSRulKNb9E5o4p/QK6/Fe0WyhpVI6bxORZK7SD+pELn0t7ZqTd25raT35mrZL/TUPifEKODGf6AcAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMTEtMDJUMTQ6NTc6MjkrMDA6MDD4t/0HAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTExLTAyVDE0OjU3OjI5KzAwOjAwiepFuwAAAABJRU5ErkJggg=="
        }
    ]
}
```

## Market List

> - **Authentication required**: Yes
> - **Rate limits**: 12 every hour
> - **Recommended limit**: The market list is cached for about ten minutes. More frequent requests are unnecessary.

Returns the list of currently open shops with their contents.

```
GET /market/list
```

```
curl --header "x-api-key: <your_api_key>" https://api.arcadia-online.org/api/v1/market/list
```

#### Response Fields

| Name                | Type      | Value                         |
|:--------------------|:----------|:------------------------------|
| `shops`                         | Array    | The list of shops.                                                                                            |
| `shops[i]`                      | Object   | Each shop currently open.                                                                                     |
| `shops[i]/title`                | String   | The shop title, as visible in the in-game message bubble.                                                     |
| `shops[i]/owner`                | String   | The shop owner's name, as visible under the character in-game.                                                |
| `shops[i]/location`             | Object   | The shop location.                                                                                            |
| `shops[i]/location/map`         | String   | The map name (as visible in-game through the `/where` command).                                               |
| `shops[i]/location/x`           | Integer  | The x coordinate of the location (as visible in-game through the `/where` command).                           |
| `shops[i]/location/y`           | Integer  | The y coordinate of the location (as visible in-game through the `/where` command).                           |
| `shops[i]/creation_date`        | Date     | The shop opening date and time.                                                                               |
| `shops[i]/type`                 | String   | The shop type. Possible values: `V`: Vending shop; `B`: Buying Shop.                                          |
| `shops[i]/items`                | Array    | The shop contents.                                                                                            |
| `shops[i]/items[j]`             | Object   | Each item in the shop.                                                                                        |
| `shops[i]/items[j]/item_id`     | Integer  | The numeric item identifier. Matches the `item_id` field of the [Item Database](#item_db).                    |
| `shops[i]/items[j]/amount`      | Integer  | The amount of the item that is available in the shop.                                                         |
| `shops[i]/items[j]/price`       | Integer  | The price the item is listed at, in Zeny.                                                                     |
| `shops[i]/items[j]/refine`      | Integer? | The item's refinement level. Only present if greater than zero.                                               |
| `shops[i]/items[j]/cards`       | Array?   | The cards slotted into the item. Only present if the item contains at least one card.                         |
| `shops[i]/items[j]/cards[k]`    | Integer  | The item ID of each card slotted into the item. Matches the `item_id` field of the [Item Database](#item_db). |
| `shops[i]/items[j]/star_crumbs` | Integer? | For forged items, the amount of Star Crumbs used to forge it. Only present if greater than zero.              |
| `shops[i]/items[j]/element`     | String?  | For forged items, the item's element. Only present if the item is an elemental weapon. Possible values: `Ice`, `Earth`, `Fire`, `Wind`. |
| `shops[i]/items[j]/creator`     | Integer? | For forged or brewed items, the item creator's unique identifier. It can be compared against the [Fame List](#fame_list) to know if the item is ranked. Only present for forged or brewed items. |
| `shops[i]/items[j]/beloved`     | Boolean? | For pets, whether the pet is named. Only present for named pet eggs.                                          |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "shops": [
        {
            "title": "Hello World!",
            "owner":"A certain Merchant",
            "location": {
                "map": "prontera",
                "x": 150,
                "y": 150
            },
            "creation_date": "2019-11-21T22:31:39Z",
            "type": "B",
            "items": [
                {
                    "item_id": 984,
                    "amount": 310,
                    "price": 3500
                }
            ]
        },
        {
            "title": "Selling Stuff",
            "owner": "Another merchant",
            "location": {
                "map": "prontera",
                "x": 130,
                "y": 140
            },
            "creation_date": "2019-11-21T22:31:01Z",
            "type": "V",
            "items": [
                {
                    "item_id": 2231,
                    "amount": 1,
                    "price": 1200000
                },
                {
                    "item_id": 1622,
                    "amount": 1,
                    "price": 3200000,
                    "refine": 5,
                    "cards": [ 4004, 4004 ]
                },
                {
                    "item_id": 12006,
                    "amount": 4,
                    "price": 9999
                },
                {
                    "item_id": 1360,
                    "amount": 1,
                    "price": 400000,
                    "refine": 5,
                    "star_crumbs": 2,
                    "element": "Wind",
                    "creator":150330
                }
            ]
        }
    ]
}
```

## Fame List

> - **Authentication required**: Yes
> - **Rate limits**: 12 every hour
> - **Recommended limit**: The fame list is cached for about ten minutes. More frequent requests are unnecessary.

Returns the list of famous brewers and forgers, as available in-game through
the `/alchemist` and `/blacksmith` commands.

```
GET /fame/list
```

```
curl --header "x-api-key: <your_api_key>" https://api.arcadia-online.org/api/v1/fame/list
```

#### Response Fields

| Name                | Type    | Value                                                                                    |
|:--------------------|:--------|:-----------------------------------------------------------------------------------------|
| `brewers`           | Array   | The brewing fame list.                                                                   |
| `brewers[i]`        | Object  | Each brewer in the fame list.                                                            |
| `brewers[i]/id`     | Integer | The brewer's unique ID. Matching the `creator` field in the [Market List](#market_list). |
| `brewers[i]/name`   | String  | The brewer's name.                                                                       |
| `brewers[i]/points` | Integer | The current amount of fame points the brewer has.                                        |
| `forgers`           | Array   | The forging fame list.                                                                   |
| `forgers[i]`        | Object  | Each forger in the fame list.                                                            |
| `forgers[i]/id`     | Integer | The forger's unique ID. Matching the `creator` field in the [Market List](#market_list). |
| `forgers[i]/name`   | String  | The forger's name.                                                                       |
| `forgers[i]/points` | Integer | The current amount of fame points the forger has.                                        |

#### Example response

```
{
    "version": "1",
    "generation_timestamp": "2019-11-21T23:31:28.605178+00:00",
    "brewers": [
        {
            "char_id": 123456,
            "name": "A certain alchemist",
            "points": 1277249
        },
        {
            "char_id": 123654,
            "name": "Another alchemist",
            "points": 1093925
        }
    ],
    "forgers": [
        {
            "char_id": 124563,
            "name": "The best forger",
            "points": 18150
        },
        {
            "char_id": 125436,
            "name": "And another forger",
            "points":17770
        }
    ]
}
```
