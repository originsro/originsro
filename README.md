# Welcome to the Arcadia Online Bug Tracker

![Arcadia Online](.resources/topimage.jpg)

## Arcadia Online Main Website and Downloads

If you're just looking for Arcadia Online or want to download the client, please head to https://arcadia-online.org/

## Bug and Feature Tracker

If you're looking for **bug reports** or **feature suggestions** concerning Arcadia Online, this is the right place!

You can [browse the **existing issues**](https://gitlab.com/arcadia-online/arcadia-online/issues) or [create a **new one**](https://gitlab.com/arcadia-online/arcadia-online/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

If you prefer, you can see the existing issues as a [kanban board](https://gitlab.com/arcadia-online/arcadia-online/boards) or view the [development milestones](https://gitlab.com/arcadia-online/arcadia-online/milestones).

Please note that in order to post new issues, you'll need to [sign in or sign up for a free GitLab account](https://gitlab.com/users/sign_in?redirect_to_referer=yes).

![Thief Bug](.resources/ThiefBug_Female.gif)

## Reporting an Exploit & Bounties

When reporting something exploitive please mark the issue as confidential.

Some exploits of high severity make the reporter eligible for a **cosmetic bounty**, such as:
- Damage Exploits, like 1-hitting everything with ease.
- Zeny Exploits, gaining infinite Zeny with ease.
- Crashing others or the server or making the service unavailable. (Except for DDoS attacks)

**The severity of each bug will be evaluated by the staff**

## Support and Player Reports

If you want to open a **support ticket** or **report a player**, please head to our [Help Desk](https://gitlab.com/arcadia-online/arcadia-support)

## Technical Support

If you're looking for **technical support** [Forum](https://bbs.arcadia-online.org/) or [Discord Server](https://arcadia-online.org/discord).

## Arcadia Online useful links

- Main website: https://arcadia-online.org
- Forum: https://bbs.arcadia-online.org
- Control Panel: https://cp.arcadia-online.org
- Wiki: https://wiki.arcadia-online.org
- Bug and Feature Tracker: https://gitlab.com/arcadia-online/arcadia-online
- Help Desk: https://gitlab.com/arcadia-online/arcadia-support
