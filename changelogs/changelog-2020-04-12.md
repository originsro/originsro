# Update Changelog - 2020-04-12 (part of Ep. 10.2)

## Changelog for Players

### Added

- **Events**: Added content related to the Easter 2020 event. See the Lunatic in Prontera to get started. Note: each reward can be claimed only once per Master Account.
- **Commands**: Added the `/aura2` command, to disable the "bubble effect" (secondary aura). In order to fully disable both auras, `/aura` and `/aura2` can be used.

### Changed

- **Skills**: Corrected the duration and improved the display of Pneuma's visual effect.
- **Skills**: Changed Hiding and Magnum Break to no longer reset the idle timer, as they were easily abusable by macro users. originsro/originsro#1905
- **Mobs**: Increased the respawn timer on the custom Incubus and Succubus spawns of Geffen Dungeon 4, easily abusable by people attempting RMT. The official spawns in Geffenia are not affected by this change.
- **Misc**: Improved performance of the hourly backups.
- **Maps**: Removed the minimum distance between chats in WoE/GvG/PvP maps. originsro/originsro#1220 originsro/originsro#1870
- **Maps**: Updated the no-vending areas in Payon, Archer Village and Geffen.
- **Misc**: Changed the login announcer message to show the actual online player count, excluding autotraders.
- **Misc**: Improved the performance and display of some visual effects.
- **Commands**: Improved the performance and visual display of Grimtooth and Status Recovery when `/mineffect` is enabled (or during WoE).
- **Misc**: Improved performance of the client, minimizing disk read/write operations.
- **Skills**: Improved the Weapon Refine window, including information about the current refine level and cards.

### Fixed

- **Skills**: Fixed some rare cases where the status icons are displayed without a duration even if they should have one.
- **Skills**: Fixed some visual glitches caused by Kaite.
- **Skills**: Fixed the excessive delay after the Counter Kick Stance is triggered.
- **Skills**: Fixed the DEF recalculation of the Eska skill not happening every second as it should.
- **Items**: Fixed an issue that could leak the presence of a character with a Maya Purple card to nearby characters.
- **Skills**: Fixed an issue in the Weapon Refine skill, excessively reducing the success rate at job levels lower than 50.
- **Misc**: Improved game performance in highly populated areas, during the day/night switches.
- **Skills**: Fixed an issue that caused Super Novices to lose no-deaths status if Steel Body automatically activates. originsro/originsro#1897
- **Skills**: Fixed the Platinum Skill NPCs for certain job classes not allowing to re-learn the skills after a character re-rebirths. originsro/originsro#1921
- **Mobs**: Fixed the display name of the Biolabs 3 MVPs.
- **Quests**: Corrected the name of the Rest skill in the Alchemist job change quest (previously referred to as Vaporize). originsro/originsro#1907
- **Quests**: Removed a duplicated warp portal in the Swordsman job change map. originsro/originsro#1914
- **Items**: Removed the unintended trade restrictions from the Well-Chewed Pencil Costume.
- **Pets**: Fixed an error in the pet capture rate calculation, causing a higher than intended success rate for very low level characters.
- **Skills**: Fixed an error causing Dispel to be able to remove the effect of bard/dancer ensembles from a character standing into them.
- **Skills**: Fixed the Mental Charge skill persisting after the Lif is put to Rest and not allowing to be recast after the homunculus is called again. originsro/originsro#1892
- **UI**: Fixed an issue with the party booking announcements when advertising for Abyss Lakes or the Hugel Fields.
- **Items**: Fixed the Leib Olmai card's adjective Red Stone showing up as a suffix instead of a prefix. originsro/originsro#1799
- **Client**: Fixed the copy and paste functionality when using non-English characters.
- **Client**: Fixed the mouse cursor when leaving `@duel`.
- **Skills**: Fixed the Assumptio and Power Up display on monsters when entering their sight range after they have the effect on.
- **Misc**: Fixed various visual glitches with falcons.
- **Items**: Fixed an issue that caused Arbalest, Rudra Bow and Crossbow not to display the arrow animations.
- **UI**: Fixed a rare crash when dragging the party window.

This update contains 74 commits.
