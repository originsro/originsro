# Update Changelog - 2019-10-31 (part of Ep. 10.1)

#### OriginsRO Episode 10.1 Content

- **Maps**:    Added the Somatology Laboratory dungeon level 3.
- **Maps**:    Added the Robot Factory dungeon.
- **Maps**:    Added Geffenia.
- **Classes**: Added the Rebirth classes and the related quest. The following classes are now playable:
  - High Novice
  - High Swordsman
  - High Magician
  - High Archer
  - High Acolyte
  - High Merchant
  - High Thief
  - Lord Knight
  - High Priest
  - High Wizard
  - Whitesmith
  - Sniper
  - Assassin Cross
  - Paladin
  - Champion
  - Scholar
  - Stalker
  - Biochemist
  - Minstrel
  - Gypsy
- **Mobs**:    Added the monsters spawning in the newly introduced maps.
- **Items**:   Added the items related to the newly introduced maps and quests, including several items that were previously disabled and unequippable.
- **Mobs**:    Relocated and rebalanced the monster spawns worldwide. The spawns now roughly match the official ones, with various improvements.
- **Quests**:  Added various new quests:
  - Pickpocket Quest
  - Cursed Spirit Quest
  - Kiel Hyre Quest
  - Level 4 Weapon Quests
  - The Sign Quest
- **NPCs**:    Extended the Socket Enchant NPCs with new items:
  - Assassin Dagger
  - Berserk
  - Dragon Killer
  - Dragon Slayer
  - Ginnungagap
  - Grand Cross
  - Gungnir
  - High Heels
  - Infiltrator
  - Katar of Frozen Icicle
  - Katar of Piercing Wind
  - Katar of Quaking
  - Katar of Raging Blaze
  - Mage Coat
  - Memory Book
  - Pantie
  - Poison Knife
  - Robe of Cast
  - Schweizersabel
  - Sucsamad
  - Survivor's Rod (DEX)
  - Survivor's Rod (INT)
  - Undershirt
  - Zephyrus
- **Items**:   Added 10 new costumes:
  - *200k z*
    - Cigarette Costume
    - Goggles Costume
    - Rainbow Eggshell Costume
    - Wizard Hat Costume
  - *380k z*
    - Green Feeler Costume
    - Wonder Nutshell Costume
    - Striped Hairband Costume
    - Circlet Costume
  - *500k z*
    - Mystic Rose Costume
    - Puppy Love Costume
    - Feathered Hat Costume
    - Jack be Dandy Costume
  - *750k z*
    - Evil Wing Costume
    - Pretend Murdered Costume
    - Renown Detective Cap Costume

#### Added

- **Events**:   Added the Halloween Event. See the Suspicious Coffin in Prontera.
- **NPCs**:     Added a stat refunder NPC, allowing to refund up to 33 status points, once every 3 months. For details, see the Physiotherapist NPC in Comodo.
- **Commands**: Added the `@cl` command, alias for `@channel list`.
- **Misc**:     Added the fame scaling system. The fame points of all brewers, forgers and ranker Taekwons will be reduced by 2% each month, starting on December 1st, 2019. This does not affect the relative positions in the rank, since it applies to every character in the same proportion.
- **Chat**:     Added a `#ru` chat channel, for Russian language chat.
- **Commands**: Added a new `@nores` command, to prevent enemies (in GvG or PvP maps) or players outside the party (in other maps) from resurrecting a dead character.
- **Commands**: Added a message on login explaining the reason why a merchant was disconnected or muted, in case of Vending or Buying Store ToS infringements.
- **Commands**: Added a new command `@blockenemy`. The command is similar to `@noask`, but it only blocks enemies and prevents accidentally joining chat rooms in PvP and GvG.
- **Commands**: Added a new command `@ping`, to measure the real latency between client and game server.
- **Commands**: Added support for sessions to the commands `@expgain`, `@exp` and `@playtime`. It's possible to reset the session, and restart counting from zero, by calling `@expgain reset`, `@exp reset` (or `@expgain clear`, `@exp clear`), without having to log out and back in.
- **Items**:    Added skin tone variants of the Elven Ears and Elven Ears Costume. The item will adjust its color to match the skin tone of the character that wears it.
- **UI**:       Added a message to warn about possible GM impersonators when creating or joining a chat room.
- **Commands**: Added a new command, `/quake`, to disable the screen shaking effect.
- **Client**:   Extended the chat lock (the padlock icon on the top right corner of the chat window) to prevent accidentally pressing the + and - buttons to add/remove chat tabs.

#### Changed

- **Misc**:     Transferred the game server to faster hardware, for improved performance in peak periods. Failover connection and APAC proxy will be available again shortly.
- **Misc**:     Improved performance when logging in and out and when opening the Kafra storage.
- **Maps**:     Added new no-vending areas in Comodo and Geffen.
- **Items**:    Strawberry and Assassin Dagger have been restored to their official version.
- **Skills**:   Ground-based buffs and de-buffs are now cleared when going through a Warp Portal (Acolyte skill) or the wedding recall skills. The following buffs and de-buffs are currently affected:
  - Songs (A Whistle, Assassin Cross of Sunset, A Poem of Bragi, The Apple of Idun)
  - Dances (Humming, Please Don't Forget Me, Fortune's Kiss, Service for You)
  - Immunity to altered statuses (Gospel)
  - Max HP +100% (Gospel)
  - Max SP +100% (Gospel)
  - All stats +20 (Gospel)
  - DEF +25% (Gospel)
  - ATK +100% (Gospel)
  - HIT +50 (Gospel)
  - Flee +50 (Gospel)
  - DEF -100% (Gospel)
  - DEF -20% (Tarot: The Sun)
  - ATK -50% (Tarot: Strength, Tarot: The Devil)
  - ATK -20% (Tarot: The Sun)A
  - ATK -100% (Gospel)
- **UI**:       Items from the inventory's Favorite tab are no longer listed in the list of items available to sell to an NPC shop, to prevent accidentally selling them.
- **Maps**:     The Einbech town can now be memorized as Warp Portal destination.
- **Exp**:      Reduced the non-multiclienting party bonus in favor of an increased tap bonus.
- **Skills**:   Changed most skills to be able to reset the idle timer again. Notable exceptions are:
  - Teleport
  - Item Appraisal
  - Vending
  - First Aid
  - Change Cart
  - Crazy Uproar
  - Cast Cancel
  - Magic Rod
- **Commands**: Improved the option handling in the `@noks` command:
  - A new option `@noks off` has been added, to disable noks regardless of its current state
  - A new option `@noks auto` has been added, behaving like `@noks party` if used when in a party or `@noks self` otherwise.
  - Using `@noks` a second time with the auto, party, self, guild options no longer disables the protection.
- **WoE**:      Changed the Emperium stats to an alternative version with lower Vit and higher HP.
- **Items**:    When a Buying Store fails to open (for example, in a cell where Vending is disallowed), the permit is no longer consumed.
- **Maps**:     Relocated the Comodo Inn to a new building, since it unintentionally overlapped with the Southern Branch of the Kafra Corporation, which provided the Kafra Storage service, unlike every other inn. The Repair NPC has also been moved accordingly.
- **Skills**:   Renamed the Weapon Repair skill to Equipment Repair.
- **Skills**:   Changed the cooldown of the Homunculus skills (except Urgent Escape) to be reset when the homunculus is vaporized.
- **NPCs**:     Streamlined PvP access:
  - Less clicks are necessary to check the amount of players in the PvP maps.
  - Information about the PvP arenas is displayed in a simpler manner.
  - The options provided in the PvP menu are less confusing.
  - The PvP NPC is bigger and more noticeable.
  - It's now possible to access either the lobby or the arenas, from the NPCs in town.
- **Battle**:   Improved combat timing and synchronization between client and server, fixing most causes of position lag.
- **NPCs**:     Increased the Brave Warrior donation requirement to 50M Zeny.
- **Misc**:     Improved the bot detection system.
- **WoE**:      Increased the drop rate of the castle-specific Treasure Chests to 2x.
- **Mobs**:     Renamed the Green Maiden standalone monster to Chun Yi, to prevent ambiguities. The White Lady's summon will stay as Green Maiden.
- **Mobs**:     Added Anubis to the MotD blacklist.
- **Mobs**:     Reverted the Swordsman Egnigem Cenia monster name to its gender-neutral version.
- **Items**:    Added more flavor to the description of the Kafra Band.
- **Misc**:     Updated the palettes of Novice and 1-1 classes (including rebirth classes) for a better appearance. First classes are now limited to a curated selection of 50 palettes instead of loading colors that were meant for 2-1 and 2-2 classes.
- **Client**:   Improved the client loading screen time, by up to 16 times.
- **Client**:   Changed the Compare inventory checkbox to remember its state when closing and reopening the client.
- **Client**:   Changed the item names to always include the amount of slots (even if 0) to prevent many accidental or intentional scams.
- **Client**:   Improved client performance and memory usage in Windows when multi-clienting.
- **Client**:   Improved the visual quality of the JPG screenshots captured through the client's screenshot function (PrintScreen).
- **Client**:   Improved the screenshot performance with nVidia GPUs. Screenshots will no longer freeze the client for a second.
- **Client**:   Extended the maximum length of the in-game command shortcuts. The Super Novice chant will now fit correctly regardless of the character's name.

#### Fixed

- **Misc**:     Fixed some rare issues occurring when a storage or buying/vending store was open.
- **Party**:    Fixed a rare condition that could allow exp sharing in a party that doesn't satisfy all the requirements.
- **Items**:    Fixed visual glitches in the characters' appearance when wearing multiple overlapping headgears and costumes.
- **NPC**:      Fixed an error in time formatting in the Brave Warrior NPC during the last 24 hours of cooldown.
- **Quests**:   Fixed some typos in the How the Airship Works quest.
- **Items**:    Fixed the Wanderer Sakkat Costume item incorrectly claiming to have a slot.
- **Skills**:   Fixed Hocus Pocus disregarding Ice Wall map restrictions.
- **NPCs**:     Fixed an exploit that could potentially allow to bypass the floating rates event cooldown.
- **Quests**:   Fixed an issue that prevented characters that learned the First Aid quest in the Training Grounds from completing the Play Dead quest.
- **WoE**:      Fixed an issue that could allow castle owners to keep their homunculi in pre-trans WoE.
- **Items**:    Fixed the Infinitus Potions not working in GvG Certamen.
- **Pets**:     Fixed an issue that caused certain pet or homunculus names to be rejected as invalid.
- **Chat**:     Fixed an issue that prevented the channel topic from being displayed when joining a chat channel.
- **Skills**:   Fixed the Homunculus skill requirements being enforced on the owner as well. It is now possible to use Homunculus skills even if the owner is overweight and doesn't have enough SP, as long as the homunculus does.
- **Skills**:   Fixed the Homunculus skill failure message not displaying the required item, for skills that consume an item.
- **Skills**:   Fixed the Chaotic Blessings skills never selecting the enemy as healing target.
- **Mobs**:     Fixed an issue that could cause a monster to get stuck walking to and targeting a dead character.
- **Misc**:     Fixed an issue that could cause a ranged weapon user to be unable to cancel a walk action until it reached its original destination.
- **Mobs**:     Fixed MVP/boss slaves not automatically switching to chase or target their master's target.
- **Skills**:   Fixed the Gunslinger skill Bull's Eye skill not working on Demi-Human monsters.
- **Skills**:   Fixed the cost of the Ninja skill Throw Coins to be affected by damage reductions.
- **NPCs**:     Fixed a possible exploit in the Izlude Arena.
- **Skills**:   Fixed the Spear Mastery damage only getting applied to Pierce once regardless of the amount of hits.
- **WoE**:      Fixed the Emperium positions in al the castles to be as close as possible to the center of the podium.
- **Attack**:   Fixed various issues with equipment or equipment-granted buffs not taking effect if switched or triggered while being attacked.
- **Skills**:   Fix visual glitches caused by song and dance skills, leaving parts of their effect behind after terminating.
- **Maps**:     Corrected the BGM for the Swordsman Job Change map.
- **Commands**: Fixed a typo in the `@hidepetmsg` command.
- **Items**:    Fixed invisible shields or incorrect dual-wielding sprites.
- **Commands**: Replaced the Unknown Error message displayed when using `@autotrade` with the correct Disconnected from Server message.
- **Misc**:     Improved the support for the skin tone system, with better compatibility with NPCs (Tailor, Hair Dyer) and quests (Skin Clinic, Valkyrie).
- **Mobs**:     Fixed an issue that caused Eddga to visually reappear to characters affected by External Bleeding, after the MVP died.
- **WoE**:      Fixed the castle entrance warps to behave like normal warps (ignoring cloaked characters) and fix a possible way to abuse them with a modified client.
- **Items**:    Fixed the description of Wickebine Tres Card and The Paper Card to mention Snatch, not Plagiarism.
- **Maps**:     Corrected the location of some mini-map icons.
- **UI**:       Fixed the Master Storage and Guild Storage title getting visually cut off.
- **Skills**:   Fixed the character's appearance when walking with Energy Coat, Two-Hand Quicken, etc. and a custom cloth color.
- **UI**:       Fixed the message "X is not your guild's member" unnecessarily getting displayed in several situations.
- **Items**:    Fixed the swing animation for the weapons that support it.
- **Items**:    Fixed a conflict between certain items and skills on the shortcut skill bars. It's now possible, for example, to put White Potion and Madness Canceller on the same skill bar.
- **Client**:   Fixed an incompatibility with recent nVidia GTX graphics cards. Note: it's now recommended to disable or uninstall the dgVodoo third party patch, since it is known to cause other problems and is no longer necessary now.

#### Removed

- **Events**: The Gunslinger vs Ninja event is now officially closed, and the reward NPCs have been removed. Thank you for participating!

This update contains 572 commits.

## 2019-11-01 (Dev, ep. 10.1 supplementary update)

- **UI**:       Fixed an issue with the hair and skin color on the guild member list.
- **Misc**:     Fixed some false positives in the bot detection system.
- **Commands**: Enabled the `@ping` command.
- **Chat**:     Added the `#es` chat channel.
- **NPCs**:     Fixed a typo in the Assassin Dagger name in the Socket Enchanter.
- **Events**:   Improved the drop rate of the Organic and Non-Organic Pumpkin for the Halloween Event.
- **Maps**:     Fixed an issue that caused crashes in the Hidden Temple map.
- **Quests**:   Replaced "GMT" with "UTC" in the Sign quest.

This update contains 11 commits.

## 2019-11-02 (Dev, ep. 10.1 supplementary update 2)

- **NPCs**:   Added a confirmation message to the Valkyrie, to avoid the risk to accidentally re-rebirth as a level 1/1 High Novice.
- **Items**:  Fixed an issue that caused costume headgears to be visible in WoE.
- **Items**:    Fixed the Infinitus Potions not working in GvG Certamen (again).
- **Mobs**:   Extended the position-lag fixes to the Mi Gao splits, that were previously excluded.
- **Events**: Fixed a problem that allowed the Halloween event's Mischievous Ludes to occasionally teleport a merchant.

This update contains 8 commits.
