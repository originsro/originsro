## 2024-11-16 (Maintenance)

### Changelog for Players

#### General
- **Halloween Event!** check it out: https://wiki.arcadia-online.org/Halloween_%282024%29/
- **Relaunch Event still running**, check it out: https://wiki.arcadia-online.org/Relaunch_2024/

#### Known Issues
- **Client**: The game prefers to be run in `C:/Users/myuser/arcadia-online`, any other folder may not have the needed permissions.
- **Misc**: Running the game in a folder that is controlled by OneDrive will result in crashes.
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**, **Client**: Running any of the game binaries as administrator is highly discouraged, in the future our binaries will be tsuntsun about it and refuse to run as it might kill the entire world's kittens.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll`, `libssl-3-x64.dll` or `qopensslbackend.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it inside the launcher's folder fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.
- **Client**: Under rare circumstances the client can *steal your mouse* resulting in you not seeing your cursor, using `windows key` + `L` and logging back in fixes it.

#### Added
- **Quests**: Added permanent NPC for `Pizza in Mouth`. [ca9b788fcd, 5bdb3156dd, 65af99cbe8]
- **Events**: Added missing (Non-)Organic Pumpkins to maps deleted by Renewal *which we aren't*. [52400575f3]
- **CP**: Added masteraccount/linkdiscord page for linking MA <-> discord acc. [F:607146880d]
- **CP**: Added support for discord in masteraccount/view. [F:f378fd7dc5, F:a46537c0b3]
- **Items**: Added `Halloween Box` to client's buying store item list. [C:ed431bb513]

#### Changed
- **Events**: Replaced `Bathory` exclusion with `Enchanted Peach Tree` exclusion in `Mob of Day`. [a6263b523e]
- **NPCs**: Ensured that Ceremony of Marriage in Prontera isn't started for same-sex couples, as it doesn't work. [4a7bcfd143, fb0ada03a3, ea31879149]
- **UI**: Renamed Emotion Tab in Keyboard Settings to Other to represent its meaning better. [C:9fa06f9fee]
- **Misc**: Changed fallback from target position to standing position for monsters' desired path when determined unreachable by the client. [C:c31459361a]

#### Fixed
- **WoE**: Fixed an issue with the pre-trans rotation that let to the previous castle not being cleared of its owner. *Respect to the guild that didn't abuse this even for a second* [02269a5785]
- **Quests**: Fixed some incorrect item IDs in the Juperos Ruins quest resulting in the wrong plates being deleted. Related to arcadia-online/arcadia-online#3921 [86b6c9da83]
- **Misc**: Fixed crashes when minimap zoomed in and teleporting. [C:23fb7466d7]
- **Misc**: Fixed same Master account trades resulting in visually wrong zeny values under some conditions. [C:096b25b26c]
- **Misc**: Fixed a rare crash when using a button such as feeding your homunculus. [C:1f18c5db60]
- **Misc**: Fixed a rare crash when changing to character selection. [C:a218d48535]
- **Items**: Fixed Sweet Gent and Cowboy Hat sprite borders. [B:6567b124e0]
- **Items**: Fixed magenta pixels on Tuxedo icon. [B:b29170f2bd]
- **Items**: Fixed blue, yellow, pink, brown, gray drooping cat sprites. [B:3ad85f2822]

#### Removed
- **Events**: Removed Skogul and Frus from Mob of Day. [9236125483]
- **Events**: Removed Gremlin from Mob of Day until natural spawns exist outside Sideworld. [84a247fb66]
- **Events**: Excluded eligible spawns from `Mob of Day` criteria which have a respawn time greater than 5 minutes (including variance). [d229122a78]
- **Items**: Removed Clockwork Hat spark animation. [B:fd446481b1]

> This update consists of 54 commits.
