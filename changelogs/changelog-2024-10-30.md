## 2024-10-30 (Maintenance)

### Changelog for Players

#### General
- **Halloween Event!** check it out: https://wiki.arcadia-online.org/Halloween_%282024%29/
- **Relaunch Event still running**, check it out: https://wiki.arcadia-online.org/Relaunch_2024/

#### Known Issues
- **Client**: The game prefers to be run in `C:/Users/myuser/arcadia-online`, any other folder may not have the needed permissions.
- **Misc**: Running the game in a folder that is controlled by OneDrive will result in crashes.
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**, **Client**: Running any of the game binaries as administrator is highly discouraged, in the future our binaries will be tsuntsun about it and refuse to run as it might kill the entire world's kittens.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll`, `libssl-3-x64.dll` or `qopensslbackend.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it inside the launcher's folder fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.
- **Client**: Under rare circumstances the client can *steal your mouse* resulting in you not seeing your cursor, using `windows key` + `L` and logging back in fixes it.

#### Added
- **Mobs**, **Maps**: Added Pumpkins to Umbala dungeon. [f063764826]
- **Mobs**, **Maps**: Added Pumpkins to Juno field 5. [03f8ee4db5]
- **UI**: Added last position minimap symbol for 20 seconds when teleporting on same map. [C:424c868a58, B:89bd5986f6]
- **UI**: Added Halloween 2024 Encyclopedia button under Event Tab. [c1800220f8]

#### Changed
- **Items**, **Events**: Buffed chances of `Cat Ears Beret Costume` and `Clockwork Cap Costume` in `Halloween Box`. [b3213c2d6d]
- **Items**: Made halloween boxes buyable. [c403edbe2e]
- **Items**: Changed sprite of Blank Eyes to work generally better with all skins and hairstyles. [bc6cbdbf04, C:ecc23c4810, C:760d42dde4, B:f27519762f, B:45a1e65219]
- **Misc**: Made trades auto-accept when requesting between same Master Account. [782724bbfd]
- **Misc**: Made trade auto-complete when between same Master Account and pressed OK on one side. [62f83cd067]
- **Misc**: Made trade auto-continue between same Master Account as long as at least one side wants to. [078b77d25c]
- **Events**, **NPCs**: Made Halloween 2013 NPC exchange as many Pumpkins as possible. [b7f6ea681b, 8b4b7cce3e]

#### Fixed
- **Events**: Fixed Schwarzwald Outlaw not showing kill time. [01535f8de5]
- **Commands**: Fixed `/hidedmgnums` not hiding blue crit bubbles used by Taekwon Spells. [C:b4c63a09bb]
- **UI**: Fixed ammunition of other players appearing on equipment's viewed (when they have no ammunition). [C:009f3d2ba2]
- **Items**: Fixed file issue with `Pizza in Mouth` collection image. [B:f186dfdd43]
- **Items**: Fixed `Bolt Ears Costume`. [B:1047e8b3d0]
- **UI**: Fixed worldmap tab-view fix not working. [B:de0be676bd]

#### Removed
- **Misc**: Removed idle players from loot protection. [f7c0c04e9c]
- **Maps**: Removed sideworld mapflag from Cracked Dimension house that gets you to sideworld. [a124ff310e]

> This update consists of 34 commits.
