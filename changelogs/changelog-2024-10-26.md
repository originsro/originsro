## 2024-10-26 (Halloween Event)

### Changelog for Players

#### General
- **Halloween Event!** check it out: https://wiki.arcadia-online.org/Halloween_%282024%29/
- **Relaunch Event still running**, check it out: https://wiki.arcadia-online.org/Relaunch_2024/

#### Known Issues
- **Client**: The game prefers to be run in `C:/Users/myuser/arcadia-online`, any other folder may not have the needed permissions.
- **Misc**: Running the game in a folder that is controlled by OneDrive will result in crashes.
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**, **Client**: Running any of the game binaries as administrator is highly discouraged, in the future our binaries will be tsuntsun about it and refuse to run as it might kill the entire world's kittens.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll` or `libssl-3-x64.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it inside the launcher's folder fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.
- **Client**: Under rare circumstances the client can *steal your mouse* resulting in you not seeing your cursor, using `windows key` + `L` and logging back in fixes it.

#### Added
- **Quests**: Added NPC to bypass the need of Outlaw points for `Kafra Head Band` quest. [297f63ec0d]
- **Mobs**, **Events**: Added one minute timer prior to Outlaw spawn. [fc3ceebb8f, daa2297453]
- **Mobs**, **Events**: Added killing time to Outlaw signs. [e2ac9a64ed]
- **Commands**: Introduced grace duration for `@noks` when non-owner gets attacked by mob. Allowing for victim to defend himself, as well as for owner to regain aggro. [b78fa6894e]
- **Items**: Added three new costume hats: `Blue Framed Glasses Costume`, `Green Spare Card Costume`, `Pizza in Mouth Costume`. [6cea8eb0bb]
- **NPCs**: Added **limited availability** NPCs for `Halloween Event` to recolor *Framed Glasses* and *Spare Card*. [ffc7e74067]
- **Items**: Added four new costume hats: [fc18ace095, C:bb6279d0e3]
  - `Bolt Ears Costume`, `Blank Eyes Costume` -> permanently available in Sideworld. [424ace9690, 3b90930157, cfd7dc0a81]
  - `Cat Ears Beret Costume`, `Clockwork Cap Costume` -> **limited availability** via `Halloween Boxes` [b46256d261, C:d4b34058c8, B:00b01d52d9]
- **Mobs**: Added *angry mode* to `bombering` mob, the mob will be less annoying until it is being attacked. [4eedf41ccd]
- **Mobs**: Added `fatigue` & `restoration` to `Bramblring`. [a4f7fda4bf]
- **Mobs**: Added `Increase AGI` to `Bombering` during follow mode. [cae864640c]
- **Mobs**, **Items**: Added `Yggdrasil Berry` drop to `Beech` (2% rate applied). [e795ca0b56]
- **Mobs**, **Items**: Added extra drop to `Carbonara` & `Angry Carbonara`. [650acaef09]
- **Mobs**: Added `Endure` skill to `Mobilus` while chasing. [d1f766d56b, b3be4e1628]
- **Events**: Added Halloween event entry to Encyclopedia. [C:b9c53a12e1, C:9feb67444e]

#### Changed
- **Items**: Reduced taming items cost by 85% in Sideworld. [a19ad53afb]
- **Commands**: Changed `@noks` to only have default & noguild setting (off|on|noguild). [9622c4e128, 349a2e61fc]
  - `@noks on`:
    - Self & own master-account chars share ownership
    - Party members share ownership
    - Guild members share ownership
  - `@noks noguild`:
    - Same as default, but guild members have no ownership (count as KS)
- **Skills**, **Mobs**: Replaced `Sticky Rice`'s `Scream` and `Frost Joke` with `Stun Attack` and `Frost Diver`. [a4db46d2a5]
- **Mobs**: Nerfed 12 selected Sideworld mobs hp (mostly dungeon mobs). [8fabfff922, 3439b60dbe, 3a91ec4901]
  - ~7500 range
    - `Lizzi`
    - `Lizzy`
    - `Lizzie`
  - ~6000 range
    - `Ashring`
  - ~5000 range
    - `Skeleton Ranger`
    - `Skeleton Fighter`
    - `Ancient Sorcerer`
    - `Dissonant Spirit`
  - ~3500 range
    - `Plague`
  - ~1000 range
    - `Beech`
    - `Bulbuk`
    - `Rodora`
- **Mobs**: Reduced `Pati` & `Rixa` hp by 2500. [08087a0838]
- **Mobs**: Reduced `Pati` & `Rixa` attack speed. [08055b9632]
- **Mobs**: Adjusted `Rixa` def and mdef. [83768c59c6]
- **Mobs**: Nerfed two skills each from `Pati` & `Rixa`. [228bf16dc2]
- **Mobs**: Increased damage motion on `Pati` & `Rixa`. [f3f94fb83e]
- **Mobs**: Nerfed Lizzi & Lizzy & Lizzie bolt skill cast time & made it interruptible. [5b49d062d0]
- **Mobs**: Reduced `marine bomb` hp by 1500. [e5962af57a]
- **Mobs**: Increased `marine bomb` `self destruction` cast time to 8 seconds. [b6a7a6abae]
- **Mobs**: Nerfed `Kyrie Eleison` skill cast chance and cooldown on `Ashring` and `Plague`. [2d95817898]
- **Mobs**: Replaced `Ashring` `cart termination` skill with `sleep attack`. [1904ef7645]
- **Mobs**: Increased `Ashring` `curse attack` cooldown to 60 seconds. [1a659b1ca7]
- **Mobs**: Replaced `Plague` `dark cross` skill with `stun attack`. [724865e589]
- **Mobs**: Increased `Plague` `wide stone` skill cooldown to 60 seconds. [e4bbbe067c]
- **Mobs**: Reduced `Lovering` & `Bittering` INT by 10 (extra stats dumped to STR). [f000ea5f5b]
- **Mobs**: Reduced `Lizzy` & `Lizzie` DEF but retains same amount for the sum of DEF and VIT (stats taken from STR). [b466d6edcd]
- **Mobs**: Fixed `Lizzie` `joint beat` skill targeting. [86512cfa85]
- **Mobs**: Replaced one spell: [33c05639c2, a511b3acbb]
  - `Lizzi`: `Lightning Spear of Ice` -> `Lightning Spear of Ice` (no longer cancelable, less chance, no cast time)
  - `Lizzy`: `Dragon Fire Formation` -> `Flaming Petals`
  - `Lizzie`: `Kamaitachi` -> `Wind Blade`
- **Mobs**: Moved `Pati` & `Rixa` self buff skill to the top of berserk state list. [bdeac4c277]
- **Mobs**: Made `Pati` & `Rixa` more blind in view range. [121303f2c8]
- **Mobs**: Increased `Pati` & `Rixa` chase range to compensate for their blindness; they are blind but they can hear you run. [cf672f7c00]
- **Mobs**: Nerfed `Bombering` `hammerfall` skill. [a0ad173239]
- **Mobs**: Nerfed `Bramblring` & slave `dark strike` skill. [58c220b8ae]
- **Mobs**: Moved `Bramblring` `dark strike` skill to top of rush mode list. [4987c25aa7]
- **Mobs**: Nerfed `Bramblring` INT and DEX stats (stats dump to STR and LUK). [3be2c81762, 887c90fc40]
- **Mobs**: Nerfed `Jackal` ATK, AGI, DEX, ASPD (stats dump into STR, INT, LUK). [64ff67899c]
- **Mobs**: Nerfed `Kirikage` from `Jackal`. [bee492b767]
- **Mobs**: Nerfed `Miu` & slave ASPD. [fe65ce15d2]
- **Mobs**: Nerfed `Miu` & slave skills (summons, `Kirikage`, `Eske`). [397e1b531f]
- **Mobs**: Adjusted `Lovering` & `Bittering` stats (mostly nerf on DEF and VIT). [fb109385db]
- **Mobs**: Changed `Lovering` to earth level 2. [e0adfaa8cc]
- **Mobs**: Decreased `Bramblring` & slave movement speed. [dbea07fe55]
- **Mobs**: Nerfed `Quagmire` cast chance from `Elder Spore`. [e3f27c20be]
- **Mobs**: Nerfed `Hammerfall` from `Savage Brute`. [5a159857ec]
- **Mobs**: Nerfed `Berserk` chance from `Savage Brute`. [418a4561ab]
- **Mobs**: Nerfed all Sideworld mob *anti skill* cast chance. New cast chance percentage as follows: [0007886c3f]

|         | single | double | triple |
| ------- | ------ | ------ | ------ |
| day     | 20     | 15     | 10     |
| night   | 25     | 20     | 15     |
| dungeon | 30     | 25     | 20     |

- **Mobs**: Nerfed `Charge` skill cast chance and cooldown from `Savage Brute` & `Skeleton Fighter`. [cb95c383fe]
- **Mobs**: Replaced `Jungle Carbine[0]` drop with `Long Barrel[1]` from `Caldering`. [4736c98112]
- **Mobs**: Nerfed `Bryonia` & `Rodora` drop rate. [558ad48263]
- **Mobs**: Raised `Pati`, `Rixa`, `Lizzi`, `Lizzie` drop rate. [31f6382106, 405d35b2fa]
- **Mobs**: Nerfed `Bittering` drop rate. [bd6af57ffa]
- **Mobs**: Nerfed `Bryonia` drop rate more. [a8034a77ab]
- **Mobs**: Raised `Ignis` drop rate. [0f78532f72]
- **Mobs**: Nerfed `Beech` drop rate. [f1588daeff]
- **Mobs**, **Maps**: Increased mob spawns for map north of Morocc Village (mrc_fild02). [b2dabbe263]
- **Mobs**: Raised `Caldering` drop rate. [e6cc917a45]
- **Mobs**: Nerfed `Dragon Frill` drop rate. [2bf045a51a]
- **Mobs**: Revamped `Caldering` stat. [9de1c381bf]
- **Mobs**: Revamped `Ignis` stat. [06335bf5eb]
- **Mobs**: Revamped `Dragon Frill` stat. [b9c8935aca]
- **Mobs**: Revamped `Caldering` skill, mob is more fire in nature. [127855b40d]
- **Mobs**: Revamped `Ignis` skill, mob is more physical fire based attacker in nature. [fc15b05883]
- **Mobs**: Revamped `Dragon Frill` skill, mob is more magical fire in nature. [7d250c90e9]
- **Mobs**: Adjusted `Caldering` drop rate. [02cc0da81e]
- **Mobs**: Raised `Dragon Frill` drop rate. [862bba1ccb]
- **Mobs**: Buffed `Caldering` offensive stats. [59fc87e726]
- **Mobs**: Buffed `Ignis` atk stat. [fdab98b2a6]
- **Mobs**: Nerfed `Baby Fang` atk. [3f1228f512]
- **Mobs**: Nerfed `Sugaring` drop rates on mutliple items. [ac4dfa09f1]
- **Mobs**: Nerfed `Sugaring` base & job exp. [7fabcc82fb]
- **Mobs**: Nerfed `Sugaring` spawn in pro_fild00. [ad5e56707c]
- **Mobs**: Decreased `Spiderweb` cast time for `Mobilus`. [161f3abfb3]
- **Mobs**: Increased `Mobilus` base & job exp. [f95d458a25]
- **Mobs**: Nerfed `Mobilus` drop rates on several items. [99573927b3]
- **Mobs**: Buffed `Mobilus` HP & MDEF. [1e0522278c]

#### Fixed
- **Commands**: Fixed `@noks` needlessly changing owner of monster, resulting in unexpected behavior when different settings are used. [bf8f02f142]
- **Skills**: Fixed `Magnum Break` not having 50% less damage on the outer ring of its AoE, to match official behavior. Related to arcadia-online#2706 [20862fdfec]
- **Items**: Fixed White Lady's taming description. Related to arcadia-online#3856 [C:73ab3702b9]
- **Items**: Fixed incorrect loadingscreen limit / count. (16 -> 65) [C:d02fa6b419]
- **Items**: Fixed naming issue with Sideworld Card. [B:8b36565063]

#### Removed
- **Skills**: Removed cooldown of `Provoke` to match official behavior. Related to arcadia-online#2456 [fae8df096d]
- **Mobs**, **Skills**, **Events**: Removed `Cloaking` from Outlaws. [a47adc4079]
- **Mobs**, **Skills**: Removed `Dragon Fear` from `Sticky Rice` mob. [35afb196b0]
- **Mobs**: Removed `Sonic Blow` from `Jackal` (impossible to cast the skill due to attack range anyways). [6332cd5600]
- **Mobs**: Removed `Curse Attack` from `Bombering`. [85277ea150]
- **Mobs**: Removed `Kyrie Eleison` skill on `Lovering` & `Bittering`. [c076ff1fa3]

> This update consists of 133 commits.