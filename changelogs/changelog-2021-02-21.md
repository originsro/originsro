## Update Changelog - 2021-02-21 (Ep. 10.3 supplementary update)

### Changelog for Players

#### Added
- **Items**: Added 14 new costumes: [8f9884eafb, c1a5f3b1ac, 725943c821, f61cc6f285]
  - Grampa Beard
  - Ph.D Hat
  - Hat of the Sun God
  - Candle
  - Nut Shell
  - Opera Phantom Mask
  - Giant Band Aid
  - Model Training Hat
  - Bride Mask
  - Cute Green Ribbon
  - Cute Red Ribbon
  - Cute Blue Ribbon
  - Cute White Ribbon
  - Catfoot Hairpin

#### Changed
- **WoE**: Disabled more trans items in pre-trans WoE. [Click here](https://gitlab.com/originsro/originsro/-/blob/master/changelogs/attachments/pre-trans-woe-item-blacklist.md) for an up to date list of the blacklisted items. [9846371664, d5766abd2b]
- **WoE**: Disabled Bleeding in pre-trans WoE from e.g. Acid Terror. [4e305730c3]
- **Skills**: Moved the shield refinement bonus of Shield Boomerang and Shield Chain to it's base damage, like on officials. This makes it scale with Card Modificators and so on. Related to originsro/originsro#2384 [92db37da41]
- **Skills**: Ensured Shield Boomerang and Shield Chain won't ignore or pierce defense, like on officials. Related to originsro/originsro#2384 [84bc2cea87]
- **Skills**: Implemented official After Cast Delay of Arrow Vulcan. (Which is shorter) Related to originsro/originsro#1969 [76319c34be]
- **Skills**: Implemented official status-related Golden Thief Bug card behavior. Statuses inflicted by non-magical skills will go through. Related to originsro/originsro#1762, originsro/originsro#1944 [5559de0a2f, 4a0e8373cb, 949765cfd8]
- **Misc**, **Skills**: Implemented official HP & SP Regeneration behavior. [3c58616fe5, d7d4e05519, 4a30799df1, fa9b3faf14, 3a377803fe, c44ed7703e, 0a8ad2f997, 9412148457, 0e168d2621, 5db88fab73, db5770becc, 3a063773a1, f0f0f5be99, b906772706, b16f9c0a51, 03a7153637, 881ce15f3e, 89e9b8d7d6, cc7e1ecf0a, a84757053c, 89b7d741c5, d7b24b1bb9, 9c7787631e]
- **Misc**: Implemented official Confusion / Chaos behavior. [65da74ab5b]
- **Skills**: Implemented official behavior of these skills: `Romantic Rendezvous`, `Mom, Dad, I Miss You!`, `Come to Me, Honey~` and `Homunculus Resurrection`. [d80ef7780c]
- **Skills**: Made Impositio Manus always overwrite itself. [76b8ab5c2e, 7b9882e89b]
- **WoE**: Removed shadow weapon endow in pretrans castles. [c1071312fe]
- **Skills**: Implemented official chances for `Tarot Card of Fate` rolls. [bbb49481d4]
- **Skills**: Implemented official Sage & `Tarot Card of Fate` Dispel behavior. [bd5113ab17]
- **Misc**: Implemented official behavior for monster spawning and teleportation on maps, which skips a margin of the map that contains dead cells. [ea0ff4156f, bdf73a7eac]
- **Skills**: Implemented official Hocus Pocus behavior. [40fbe6958d, 323920aad9, ac1b769df4, 4ac883c19e, 01dd03ba79, 875c6cd775, a06efd71bc, 974db324b1]
- **Skills**: Stopped counting dual-clients for Kihop bonus. Only players that are not dual-clienting will be counted now. Related to originsro/originsro#2623 [c462d1b733]
- **Misc**: Prevented interacting with your bank when you can't act such as talking to an NPC or browsing a shop. [f943a634db]
- **NPCs**: Improved the user experience with the Socket Enchant NPCs. Related to originsro/originsro#2465 [c6ccaa72c2, a8e4fb979f, 1e336c872e, ca7cf4dfe0, 363c9d290f, ca4a8e4235, d04578432d]
- **Maps**: Removed christmas decorations. [91ba3fa4c7, b69e60413a]

#### Fixed
- **Skills**: Fixed Shield Boomerang and Shield Chain base damage not using entire stat point derived ATK. (Blessing and such should affect it as well) Related to originsro/originsro#2384 [aa1ed00880]
- **NPCs**: Fixed a case of the Doctor Quest taking items and not rewarding in return. Related to originsro/originsro#2389 [5d7ffa1dca]
- **NPCs**: Fixed quest log interaction and a case in which one might get stuck in Mage Job Change quest. [48b0fcbc95]
- **NPCs**: Fixed quest log interaction in Amatsu's Nine Tail quest. [479ca203ff]
- **NPCs**: Fixed quest log interaction in Lighthalzen quests. [7c874965f4, 1e55f8487a]
- **NPCs**: Fixed quest log interaction in Einbroch quests. [1da1decdec]
- **NPCs**: Fixed quest log interaction in Merchant Job Change quest. [76155900d9]
- **Mobs**: Fixed items not dropping in cases where no walkable cell is found and `@autoloot` is deactivated. (Such as the Hydras in Byalan F4 on top of the wall) [e7cc681a00]
- **Skills**: Made Magnum Break's 20% separate fire damage work with Shield Boomerang. Just like on officials. Related to originsro/originsro#1610, originsro/originsro#2384. [0f8850746a]
- **Skills**, **Items**: Fixed an exploit in the autocast system related to scrolls. [0b48b87f99]
- **NPCs**: Fixed very few NPCs not reacting to certain Jobs. Such as the Morroc Bartender. [ca39c86ef8]
- **NPCs**: Prevented farming exploits at Cursed Spirit NPC. [72841e11ef]
- **NPCs**: Prevented two rooms of The Sign quest becoming unable to be entered by anyone. [19260ddb25, 393ed8bd10]
- **NPCs**: Prevented an exploit that made multiple players able to join a solo-part of The Sign quest. [4f1e3a4615]
- **Skills**: Fixed `Tarot Card of Fate`'s SP consumption not being reduced by `Service for you` [e23cf4541a]
- **Mobs**, **Skills**: Fixed monsters that have skills above level 10 whose attributes don't scale accidentally scaling, which could cause a too high skill range for example. [038519393e]
- **Commands**: Fixed `@killcount` not working in some cases. [5a0919f193]
- **Skills**: Fixed accidentally not requiring Soul Siphon level 2 for Mind Breaker. [cd6468e43d, bfa28c077c]
- **Misc**: Fixed an exploit that allowed one to open a buying store anywhere. [571631690e]

> This update consists of 461 commits.
