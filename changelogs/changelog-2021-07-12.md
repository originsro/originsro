## Update Changelog - 2021-07-12 (Ep. 10.4 hotfix patch)

### Changelog for Players

#### Changed
- **WoE**: Changed the pretrans WoE tryout castle to Gondul (prtg_cas05).
- **Skills**, **Misc**: Implemented official ATK %, DEF %, MDEF %, MATK % buff/debuff system. Percentages of different skills are added up and form these percentage buffs/debuffs. A weaker buff/debuff cannot replace a stronger one. The following things will use it now: [9755566115, 96cabeedc8, edaf3fd188, a5ade750e3, b11661a779, 0865d2587f, d49ad9db02, ce74618763, 6d178d2188, aa74964fb7, ad268787c0, e2106845f5, 05c812de9f, 104b515497, d044bf6629, f356f84d23, d461dec15d]
  - `Tarot Card of Fate` for The Magician, Strength, The Devil and The Sun. All cards' percentages are counted as originating from `Tarot Card of Fate`. [b54ed44137]
  - `Gospel` For the 100% and -100% ATK buff/debuff [6e619c6478]
  - `Provoke` [0ee21c07a4]
  - `Concentration` skill from Lord Knight [8dfd03a0e3]
  - Amistr's `Blood Lust` [907a3e15d9]
  - `Vital Strike` [60b406dbb8]
  - `Eske` [c2bf7a9947]
  - Filir's `Flitting` [d3d33a560e]
  - The status effect *Curse* [5933b2c2f4]
  - Monster skill `Power Up` [f52ef302ad]
  - `Divest Weapon` [35375883ee]
  - `Mind Breaker` [69bebba647]
  - `Divest Shield` [bc744ff68f]
  - `Fling Coin` [f94c2c68f6]
  - `Angelus` [159d5167f9]
  - The status effects *Poison* and *Heavy Poison*, which don't stack [5732261dc3]

#### Fixed
- **Skills**: Made `Soul Exhale` never miss due to `Blinding Mist` as on officials. Related to originsro#2032 [42ecf4e973]
- **Skills**: Made `Soul Exhale` not useable on boss-type monsters as on officials. [825133f3e0]
- **Skills**: Made `Soul Exhale` not useable on frenzied characters as on officials. [e284187220]
- **NPCs**: Made the order in Dual Monster Races not matter as on officials. [7fc8bb47cb]
- **NPCs**: Corrected a recurring typo in the Monster Race NPCs. [3687b24819]
- **NPCs**: Fixed not being able to spectate Dual Monster Races. Just like on officials this will let players unglitch the Race when it goes into standby mode. [457a43e533]
- **NPCs**: Added missing name and random check for Dual Monster Races, identical to Solo Races. [b5d1c137e3]
- **Maps**, **NPCs**: Fixed Baby Super Novices not being able to move to Thanatos Tower Floor 7. [fdc3418bfc]
- **Misc**: Prevented extremely unlikely cases in which the user may be able to overwrite memory maliciously. [04421dce19, 2d64c60c18]

> This update consists of 56 commits.
