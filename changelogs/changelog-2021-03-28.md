## Update Changelog - 2021-03-28 (Ep. 10.3 supplementary update)

### Changelog for Players

#### Added
- **Event**: Added Easter Event. Visit the Cantankerous Geezer in Prontera to start the quest. [0ade6de429]
- **Mobs**: Added anti-mobbing measures for monsters in Juperos F1 in the vicinity of the warp towards Juperos F2. They will warp away if getting mobbed towards this area. [34e6d63991]
- **Misc**: Added no vending zones on stairs, bridges and pathways in Aldebaran. [c5d601791d, 0436f1b725]
- **Commands**: Added `@showrare` support for Steal. [21b8de6674, ef6681929a]
- **Commands**: Added `@droploot` support for Steal. Related to originsro/originsro#2415 [47a9cc0390]
- **UI**: Added a status icon whenever one has unread mail. Related to originsro/originsro#2646 [23a6632b32, c3a6c54479, a69d7404e7, 13a0fe013d]
- **Misc**: Added scam prevention to Buying Stores to ensure fair prices. [5029759774, ba95e8ef3c, 6b80848e7c, 933bc5f8a7]
- **Items**, **UI**: Added status icons for elemental proof potions. Related to originsro/originsro#2277 [8d4fb01f1f]

#### Changed
- **NPCs**: Improved user experience related to Arrow Quivers NPC. Related to originsro/originsro#2464 [8d3face08c, abea3fc550, 73d5cd1065]
- **NPCs**: Improved user experience related to Cartridge Box Dealer NPC. Related to originsro/originsro#2664 [9e4dbdc7f2, 4d785e7f64, 73d5cd1065]
- **NPCs**: Improved user experience related to Bullet Dealer NPC. [b82ff2888d, 73d5cd1065]
- **Commands**: Made `@noks` Protection vanish when walking or teleporting a screen away from the related monster. [9fcec91df6]
- **Skills**: Implemented official Hindsight behavior. This also fixes a damage exploit. [1921f34459]
- **Mobs**, **Skills**: Implemented official blind behavior for mobs. Related to originsro/originsro#1624 [1b96f156ed, 8fc0a83080]
- **WoE**: Removed Ginnuganap[0] from pre-trans WoE. [27e2d03bce]
- **Mobs**, **Skills**: Implemented official area behavior for monsters using Ganbantein. It always has an area of 9x9, no matter the skill level on officials. [e1e928e6de]
- **WoE**, **Skills**: Changed Bowling Bash to only hit once at 600% damage in pre-trans WoE, to match pre-trans behavior. Related to originsro/originsro#1297 [0d8507b7f1]
- **Mobs**, **Skills**: Implemented official behavior for monsters that become frozen or become stone-cursed in relation to Lex Aeterna. Related to originsro/originsro#2676 [6db68a415c]
- **Skills**: Improved the pathing of attacks via Focused Arrow Strike / Sharp Shooting and Kamaitachi. This ensures that there are no gaps in the path and a correct cell-width. [1c444b1486, 66f8da7130, 65493a1397]

#### Fixed
- **Skills**: Fixed /doridori buff of Super Novices not working. Related to originsro/originsro#2660 [02970a9403]
- **Skills**: Fixed /doridori buff of Super Novices working for *all jobs*. [02970a9403]
- **Skills**, **Mobs**: Fixed Snatch used by Monsters not teleporting a player if used on maps where players can't teleport. Such as some Thanatos Tower Flowers. [1d6d046026]
- **Homunculi**: Fixed Homunculi's natural HP & SP Regeneration being too low by accident. Related to originsro/originsro#2663 [932eff1010]
- **Quests**: Fixed a rare case in which the Kiehl quest is impossible to continue for everyone. [e5b4e29747]
- **Skills**: Fixed not being able to copy skills which you meet the pre-requirements for. Related to originsro/originsro#2641 [90540fcde7, f6cb46ae3f]
- **Skills**: Fixed Lex Aeterna being removed on players the moment Stone Curse is succesfully cast on them, instead of when it hardened. It should work just like on officials now. [6db68a415c]
- **Commands**: Lessened the burden of `@whobuys` on the server by a factor of approximately 30. [1fa0e0cbb6]
- **NPCs**, **Quests**: Fixed the experience rewards of quests being amplified by Am Mut Card by accident. [fb390aa913]
- **Skills**, **Client**: Fixed incorrect max level in skill description of `Please don't forget me`. Related to originsro/originsro#2448 [7ab9c92140]
- **Skills**, **Client**: Fixed incorrect chance in skill description of Absorb Spirits. Related to originsro/originsro#1874 [fae9e4c152]
- **CP**: Fixed displaying the wrong monsters at Star Gladiator's *Hate* targets. Related to originsro/originsro#2256 [2ea0fbb358]

> This update consists of 77 commits.
