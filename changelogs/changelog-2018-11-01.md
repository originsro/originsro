# Update Changelog - 2018-11-01 (part of Ep. 9.3)

## Changelog for Players

### Added

- **Events**: Halloween Event. See the Halloween Magician in Payon (near the guild flags) to learn how to obtain the following items:
  - Pumpkin Pie
  - Pumpkin Head
  - Magic Eyes Costume
  - Old Blue Box
  - Hockey Mask Costume
  - Old Purple Box
  - Deviling Hat Costume
  - Old Card Album
- **Events**: The Gunslinger vs Ninja event is still running.
- **CP**:     Added a Taekwon Ranking page.
- **CP**:     Added Buying Stores information to the Market section. OriginsRO#700
- **CP**:     Added Ninja and Gunslinger to the Stylist page.
- **Misc**:   Added experimental support for Market Logs, preparing the grounds for an upcoming vending/buying store market stats page. Thanks to everyone that responded to our request for comments on how to improve the data available through the CP, to avoid the need for scraping. Your comments have all been read, even if we couldn't reply to them individually (yet). Related to OriginsRO#717, OriginsRO#251

### Changed

- **Maps**:   Added Infinitus Potions to GvG Certamen.
- **Items**:  The reuse delay of the Infinitus Potions has been removed.
- **NPCs**:   Reduced the activation cost of the Floating Rates event by 40% (from 50M to 30M).
- **Stats**:  The HP tables for Ninja and Gunslinger have been updated to match the official ones, as seen in several stat calculators. OriginsRO#990
- **NPCs**:   Super Novices can now buy Buying Store licenses as if they were merchants.
- **Items**:  The Anopheles Card effect has been replaced for the current episode. The card adds a chance to drop Cheese instead of the currently useless Tentacle and Cheese Gratin.
- **NPCs**:   The position of the Cancel and All choices in the Weapon Repair NPC has been inverted, to improve ergonomics.
- **NPCs**:   The Cartridge Box Seller NPC calculates the available inventory weight in a smarter way, improving its usability when near the weight limit.
- **Items**:  Renamed Advanced Arm Guard to Improved Arm Guard.
- **Misc**:   Increased the KS Protection duration from 3s to 10s. OriginsRO#788
- **Skills**: Taekwon stances are now automatically turned off when the related skills become unavailable (such as when losing the Ranker status).
- **Skills**: Several skill name inconsistencies between game, client and CP have been corrected.
- **Items**:  The description of the Greatest General Card has been clarified.
- **Skills**: The description of Full Buster has been improved, to mention how its cooldown works. OriginsRO#1003
- **CP**:     Changed the guild list's Last Login field to use more accurate information, matching the in-game list. A tooltip on the relative date shows the absolute timestamp.
- **CP**:     Changed the alchemist, blacksmith, taekwon ranking pages to be more consistent with the in-game rankings.
- **CP**:     Added/updated icons.
- **CP**:     Re-sorted the Market (Vending) page by item, then price (from lowest).
- **CP**:     Re-sorted the Market (Buying) page by item, then price (from highest).
- **CP**:     Improved appearance of the Master Account details page.
- **CP**:     Improved appearance of the Game Account details page.
- **CP**:     Improved appearance of the Character details page.
- **Misc**:   Code optimizations and speed-ups.
- **Misc**:   Improved processing of the network traffic between server and clients, using a more appropriate system for the current population.
- **Misc**:   Improvements to the built-in cheat detection system.

### Fixed

- **Chat**:   Fixed an issue that caused members kicked from a guild to be able to receive certain `#ally` messages until logging out, depending on the alliance size. OriginsRO#956
- **Stats**:  Fixed an issue that caused the defense to become negative when a character was getting hit by more than 22 enemies. OriginsRO#726
- **Skills**: Fixed the maximum amount of coins a Gunslinger can hold, when received through the Summon Spirit Sphere skill.
- **Skills**: Fixed an issue that ignored the skill level, when auto-casting Summon Spirit Sphere through the Greatest General Card. OriginsRO#525
- **Skills**: Fixed an issue that caused Storm Gust to push its targets to a fixed direction. OriginsRO#774
- **Pets**:   Fixed an issue that caused all loyal pets to talk like a Poring, when logging in. OriginsRO#998
- **Items**:  Fixed an issue that prevented items from being usable while storage was open. OriginsRO#1044
- **Items**:  Fixed some trade restriction and weight inconsistencies in some costume headgears.
- **Misc**:   Reimplemented the OriginsRO custom party bonus calculation in a more balanced way, preventing it from stacking with Mr Kim and other future exp bonuses, since it was never intended to. It still stacks with the MotD bonus.
- **Skills**: Fixed an issue that caused hatched pet eggs to be visible (although not selectable) in the Weapon Repair list.
- **Misc**:   Fixed an issue that caused unavailability problems on the char server when quickly relogging into a character. OriginsRO#924
- **Skills**: Fixed an issue that caused the Taekwon Mission to be able to roll monsters that weren't part of the Dead Branch list.
- **NPCs**:   Fixed the Gunslinger lab director being misgendered.
- **Items**:  Fixed several misspelled words in item names, including Hvergelmir, Cotton Shirt, Angra Mainyu.
- **Items**:  Standardized the name of Gakgung.
- **Items**:  Renamed the Magazine items to Cartridge Box.
- **NPCs**:   Fixed misspellings of Tristan III.
- **Items**:  Fixed the icon of the Vane item.
- **NPCs**:   Fixed a minor possible exploit in the Juno Sage NPC.
- **NPCs**:   Fixed 'F. Harrison' not talking to a player if they are a Gunslinger that does meet the other criteria.
- **UI**:     Fixed the KS Protection Disabled message, not appearing under certain conditions.
- **UI**:     Reworded the awkward 'Your bullet is not enough' message.
- **Items**:  Fixed description of Bullets, Grenade Launcher Spheres and Packs.
- **Skills**: Fixed the description of the Hammer Fall skill. OriginsRO#489
- **CP**:     Fixed named items appearing as "Unknown's" in the cart item list.
- **CP**:     Fixed an error in the character's online time counter, when a character had never logged in.

This update contains 283 commits.

# Update Changelog - 2018-11-03 (part of Ep. 9.3, supplemental update)

### Changelog for Players

#### Added

- **Events**: Added Halloween Witch Hat to the Halloween Event.

#### Fixed

- **Shops**:  Fixed the Buying Store Merchant ignoring Super Novices.

This update contains 8 commits.
